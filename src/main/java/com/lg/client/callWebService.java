package com.lg.client;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.description.ParameterDesc;
import org.apache.axis.encoding.XMLType;

public class callWebService {
	public static QName listType = XMLType.XSD_ANYTYPE;
	public static QName stringType = XMLType.XSD_STRING;
	public static Call call;

	/**
	 * 使用Axis Call Client动态调用WebService地址.
	 * 
	 * @param webServiceAddr
	 *            WebService地址
	 * @param webServiceMethod
	 *            WebService方法
	 * @param inputNames
	 *            输入参数名称
	 * @param inputXmlTypes
	 *            输入参数XML类型
	 * @param inputValues
	 *            输入参数值
	 * @param inputJavaTypes
	 *            输入参数JAVA类型
	 * @return 成功返回<code>Object</code>, 失败或异常返回null.
	 */
	public static Object call(String webServiceAddr, String webServiceMethod,
			String inputNames, QName inputXmlTypes, Object inputValues,
			Class inputJavaTypes) {
		Object resObj = null;
		try {
			if (call == null) {
				Service service = new Service();
				call = (Call) service.createCall();
			}
			// 设置wsdl
			call.setTargetEndpointAddress(webServiceAddr);
			// 定义参数对象
			// call.setUseSOAPAction(true);
			// call.setSOAPActionURI(soapActionURI + webServiceMethod);
			// 设置访问的方法名
			call.setOperationName(webServiceMethod);
			OperationDesc oper = new OperationDesc();
			if (inputNames != null) {
				oper.addParameter(new QName(null, inputNames), inputXmlTypes,
						inputJavaTypes, ParameterDesc.IN, false, false);
			}
			oper.setReturnType(XMLType.XSD_ANYTYPE);
			call.setOperation(oper);
			// call.setTimeout(Integer.parseInt(30000));

			resObj = call.invoke(new Object[] { inputValues });
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return resObj;
	}

	public static Object callAll(String webServiceAddr,
			String webServiceMethod, String[] inputNames,
			QName[] inputXmlTypes, Object[] inputValues, Class[] inputJavaTypes) {
		Object resObj = null;
		try {
			if (call == null) {
				Service service = new Service();
				call = (Call) service.createCall();
			}

			call.setTargetEndpointAddress(webServiceAddr);

			call.setOperationName(webServiceMethod);
			OperationDesc oper = new OperationDesc();
			if (inputNames != null) {
				for (int i = 0; i < inputNames.length; i++) {
					oper.addParameter(new QName(null, inputNames[i]),
							inputXmlTypes[i], inputJavaTypes[i],
							ParameterDesc.IN, false, false);
				}
			}
			oper.setReturnType(XMLType.XSD_ANYTYPE);
			call.setOperation(oper);

			resObj = call.invoke(inputValues);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return resObj;
	}

	public static Object callAllForGBSys(String webServiceAddr,
			String webServiceMethod, String[] inputNames,
			QName[] inputXmlTypes, Object[] inputValues, Class[] inputJavaTypes) {
		Object resObj = null;
		try {
			if (call == null) {
				Service service = new Service();
				call = (Call) service.createCall();
			}
            call.setSOAPActionURI("http://tempuri.org/BillPut");
			call.setTargetEndpointAddress(webServiceAddr);
			call.setOperationName(webServiceMethod);
			OperationDesc oper = new OperationDesc();
			//oper.setName("BillPut");
//			if (inputNames != null) {
//				for (int i = 0; i < inputNames.length; i++) {
//					oper
//							.addParameter(new QName(null, inputNames[i]),
//									inputXmlTypes[i], inputJavaTypes[i],
//									ParameterDesc.IN, false, false);
//				}
//			}
			ParameterDesc param;
			param = new ParameterDesc(new QName("http://tempuri.org/", "Billstr"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
	        param.setOmittable(true);
	        oper.addParameter(param);
			//oper.setReturnType(XMLType.XSD_ANYTYPE);
	        oper.setReturnType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
	        oper.setReturnClass(String.class);
	        oper.setReturnQName(new QName("http://tempuri.org/", "BillPutResult"));
	        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
	        oper.setUse(org.apache.axis.constants.Use.LITERAL);
			call.setOperation(oper);
			resObj = call.invoke(inputValues);
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return resObj;
	}

	public static void test2() {
		String endpoint = "http://192.168.0.129:8080/services/ClientService?wsdl";
		String result = "call failed!";
		Service service = new Service();
		Call call;
		try {
			call = (Call) service.createCall();
			call.setTargetEndpointAddress(endpoint);

			call.setOperationName("getResource");
			call.addParameter("xml_request", XMLType.XSD_ANYTYPE,
					ParameterMode.IN);

			call.setReturnType(XMLType.XSD_STRING);

			List list;
			Map map;
			list = new ArrayList();
			map = new HashMap();
			map.put("grouping_name", "aa");
			list.add(map);
			list.add(map);
			list.add(map);

			result = (String) call.invoke(new Object[] { list });// 远程调用
			System.out.println(result);

		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
