package com.lg.client;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.axis.encoding.XMLType;


public class TestMain {

	public static void main(String[] args) {
		TestMain ti = new TestMain();
		 System.out.println(ti.test());
	}

	public String test() {
		List list = new ArrayList();
		Map map= new HashMap();
		map.put("money",-100);
		list.add(map)       ;
		String url = "http://localhost:8080/services/TestServiceImpl?wsdl";
		String[] inputNames = { "参数名称"};
		QName[] inputXmlTypes = { XMLType.XSD_ANYTYPE };
		Object[] inputValues = {list};
		Class[] inputJavaTypes = {List.class };
		Object ret = callWebService.callAll(url, "receiveMap",
				inputNames, inputXmlTypes, inputValues, inputJavaTypes);
		return ret.toString();
	}

}
