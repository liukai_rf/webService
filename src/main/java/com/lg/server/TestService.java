package com.lg.server;

import java.util.List;

public interface TestService {
    Object test();

    Object receiveList(List<String> strs,String name);

    Object receive(String strs);
}
