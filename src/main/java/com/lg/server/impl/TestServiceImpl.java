package com.lg.server.impl;

import com.lg.server.TestService;

import java.util.List;
import java.util.Map;

public class TestServiceImpl implements TestService {
    /**
     * 测试数据
     * @return
     */
    public Object test() {
        return "测试数据哈哈哈哈";
    }

    public Object receiveList(List<String> strs,String name) {
        String res="";
        if(strs ==null ||strs.isEmpty()){
            res="1|空数据！";
        }else {
            res="0|列表已保存！";
        }
        return res;
    }



    public Object receive(String strs) {
        String res="";
        if(strs == null || strs.equals("")){
            res="1|数据有错，服务器不做处理.";
        }else {
            res="0|已保存到360数据库。";
        }
        return res;
    }
    public Object receiveMap(List list){
        String res="";
        if(list == null || list.isEmpty()){
            res="1|数据有错，服务器不做处理.";
        }else {
            Map map =(Map) list.get(0);
            Integer monery= (Integer) map.get("money");
            if(monery == 0){
                res="1|金钱数据有错，服务器不做处理.";
            }else {
                if(monery>0){
                    res="0|已保存到360数据库（入金）。";
                }else {
                    res="0|已保存到360数据库（出金）。";
                }
            }
        }
        return res;
    }
}
